import json
from brasilprev.models import db
from brasilprev.models.produto import Produto
from brasilprev.models.produto_schema import produto_schema


def get_all():
    dados = Produto.query.all()
    return json.loads(produto_schema.dumps(dados))


def get_one(id):
    dados = Produto.query.filter_by(id=id).first()
    return json.loads(produto_schema.dumps([dados]))


def post_one(record):
    msg = "OK"
    try:
        me = Produto(**record)
        db.session.add(me)
        db.session.commit()
    except Exception as ex:
        msg = f"Falha ao inserir novo registro: {ex}"
    return msg


def put_one(record):
    msg = "OK"
    try:
        old = Produto.query.filter_by(id=record["id"]).first()
        old.name = record.get("name", old.name)
        old.price = record.get("price", old.price)
        old.description = record.get("description", old.description)
        old.image = record.get("image", old.image)
        old.stock = record.get("stock", old.stock)
        db.session.commit()

    except Exception as ex:
        msg = f"Falha na a atualização de dados: {ex}"
    return msg


def delete_one(id):
    msg = "OK"
    try:
        record = Produto.query.filter_by(id=id).first()
        db.session.delete(record)
        db.session.commit()
    except Exception as ex:
        msg = f"Falha na exclusão de dados: {ex}"
    return msg
