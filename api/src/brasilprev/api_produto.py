import time
from brasilprev.services import service_produto
from flask import request, jsonify
from brasilprev import app_brasilprev_bp


##############################################################################################################
# PRODUTO
##############################################################################################################
@app_brasilprev_bp.route('/produtos', methods=['GET', ])
def produto_get_all():
    """
    Recupera dados de todos os usuários
    :return:
    """
    try:
        retorno = service_produto.get_all()
        return jsonify(retorno)

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        return {
            "data": msg,
            "status": 500
        }


@app_brasilprev_bp.route('/produtos/<string:id>', methods=['GET', ])
def produto_get_one(id):
    """
    Recupera dados de um usuário
    :param id:
    :return:
    """
    try:
        retorno = service_produto.get_one(id)
        return jsonify(retorno)

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        return {
            "data": msg,
            "status": 500
        }


@app_brasilprev_bp.route('/produtos', methods=['POST', ])
def produto_post_one():
    """
    Inclui um novo usuário
    :return:
    """
    try:
        data = request.get_json()
        result = service_produto.post_one(data)
        return result

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        return {
            "data": msg,
            "status": 500
        }


@app_brasilprev_bp.route('/produtos', methods=['PUT', ])
def produto_put_one():
    """
    Altera dados de um usuário
    :return:
    """
    try:
        data = request.get_json()
        result = service_produto.put_one(data)
        return result

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        return {
            "data": msg,
            "status": 500
        }


@app_brasilprev_bp.route('/produtos/<string:id>', methods=['DELETE', ])
def produto_delete_one(id):
    """
    Exclui um usuário.
    :param id:
    :return:
    """
    try:
        return service_produto.delete_one(id)

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        return {
            "data": msg,
            "status": 500
        }



