from datetime import datetime
from brasilprev.models import db
from brasilprev.models.cliente import Cliente
from brasilprev.models.produto import Produto
from brasilprev.models.pedido import Pedido
from brasilprev.models.pedido_item import PedidoItem


def initialize_database():
    """
    Criação do banco e inserção de dados iniciais para teste
    :return:
    """

    db.create_all()

    # usuarios
    admin = Cliente(name="admin", password="admin123")
    guest = Cliente(name="guest", password="guest123")
    db.session.add(admin)
    db.session.add(guest)
    db.session.commit()

    # produtos
    prod1 = Produto(name="Arroz Tio João", price=12.50, stock=20)
    prod2 = Produto(name="Arroz Camel", price=9.50, stock=15)
    prod3 = Produto(name="Arroz Carrefour", price=7.28, stock=35)

    db.session.add(prod1)
    db.session.add(prod2)
    db.session.add(prod3)

    db.session.commit()

    # Pedido
    pedido1 = Pedido(date_start=datetime.now, cliente_id=1)
    db.session.add(pedido1)
    db.session.commit()

    # pedido_item
    pedido_item1 = PedidoItem(quantity=2, date_insert=datetime.now, produto_id=1, pedido_id=1)
    pedido_item2 = PedidoItem(quantity=7, date_insert=datetime.now, produto_id=2, pedido_id=1)
    db.session.add(pedido_item1)
    db.session.add(pedido_item2)
    db.session.commit()
