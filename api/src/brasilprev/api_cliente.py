import time
from brasilprev.services import service_cliente
from flask import request, jsonify
from brasilprev import app_brasilprev_bp


##############################################################################################################
# CLIENTE
##############################################################################################################
@app_brasilprev_bp.route('/clientes', methods=['GET', ])
def clientes_get_all():
    """
    Recupera dados de todos os usuários
    :return:
    """
    try:
        retorno = service_cliente.get_all()
        return jsonify(retorno)

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        return {
            "data": msg,
            "status": 500
        }


@app_brasilprev_bp.route('/clientes/<string:id>', methods=['GET', ])
def clientes_get_one(id):
    """
    Recupera dados de um usuário
    :param id:
    :return:
    """
    try:
        retorno = service_cliente.get_one(id)
        return jsonify(retorno)

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        return {
            "data": msg,
            "status": 500
        }


@app_brasilprev_bp.route('/clientes', methods=['POST', ])
def clientes_post_one():
    """
    Inclui um novo usuário
    :return:
    """
    try:
        data = request.get_json()
        result = service_cliente.post_one(data)
        return result

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        return {
            "data": msg,
            "status": 500
        }


@app_brasilprev_bp.route('/clientes', methods=['PUT', ])
def clientes_put_one():
    """
    Altera dados de um usuário
    :return:
    """
    try:
        data = request.get_json()
        result = service_cliente.put_one(data)
        return result

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        return {
            "data": msg,
            "status": 500
        }


@app_brasilprev_bp.route('/clientes/<string:id>', methods=['DELETE', ])
def clientes_delete_one(id):
    """
    Exclui um usuário.
    :param id:
    :return:
    """
    try:
        return service_cliente.delete_one(id)

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        return {
            "data": msg,
            "status": 500
        }


