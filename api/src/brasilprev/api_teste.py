import time
from brasilprev import app_brasilprev_bp


@app_brasilprev_bp.route('/teste', methods=['GET', ])
def teste():
    """
    Teste de api
    :return:
    """
    t = time.time()
    return 'Testando api brasilprev: {}'.format(t)