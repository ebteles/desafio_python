from datetime import datetime
from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///mydb/db"

db = SQLAlchemy(app)


class Clientes(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    password = db.Column(db.String(10), nullable=False)
    address = db.Column(db.String(100))
    zip_code = db.Column(db.String(10))
    city = db.Column(db.String(50))
    state = db.Column(db.String(2))
    phone = db.Column(db.String(20))
    date_insert = db.Column(db.DateTime, nullable=False, default=datetime.now)
    date_update = db.Column(db.DateTime, nullable=False, default=datetime.now, onupdate=datetime.now)

    def __init__(self, **kwargs):
        self.id = kwargs['id']
        self.name = kwargs['name']
        self.password = kwargs['password']
        self.address = kwargs['address']
        self.zip_code = kwargs['zip_code']
        self.city = kwargs['city']
        self.state = kwargs['state']
        self.phone = kwargs['phone']
        self.date_insert = kwargs['date_insert']


class Produtos(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    price = db.Column(db.Float)
    description = db.Column(db.String(500))
    image = db.Column(db.String(150))
    stock = db.Column(db.Integer)
    date_insert = db.Column(db.DateTime, nullable=False, default=datetime.now)
    date_update = db.Column(db.DateTime, nullable=False, default=datetime.now, onupdate=datetime.now)


class Pedido(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    date_start = db.Column(db.DateTime, nullable=False, default=datetime.now)
    date_end = db.Column(db.DateTime, nullable=True)
    cliente_id = db.Column(db.Integer, db.ForeignKey('Cliente.id'), nullable=False)


class PedidoItem(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    quantity = db.Column(db.Integer)
    date_insert = db.Column(db.DateTime, nullable=False, default=datetime.now)
    produto_id = db.Column(db.Integer, db.ForeignKey('Produto.id'), nullable=False)
    pedido_id = db.Column(db.Integer, db.ForeignKey('Pedido.id'), nullable=False)


# def create_database():
#     """
#     Criar banco de dados
#     :return:
#     """
#     db.create_all()
