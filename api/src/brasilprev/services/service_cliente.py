import json
from brasilprev.models import db
from brasilprev.models.cliente import Cliente
from brasilprev.models.cliente_schema import cliente_schema


def get_all():
    dados = Cliente.query.all()
    return json.loads(cliente_schema.dumps(dados))


def get_one(id):
    dados = Cliente.query.filter_by(id=id).first()
    return json.loads(cliente_schema.dumps([dados]))


def post_one(record):
    msg = "OK"
    try:
        me = Cliente(**record)
        db.session.add(me)
        db.session.commit()
    except Exception as ex:
        msg = f"Falha ao inserir novo registro: {ex}"
    return msg


def put_one(record):
    msg = "OK"
    try:
        old = Cliente.query.filter_by(id=record["id"]).first()
        old.name = record.get("name", old.name)
        old.password = record.get("password", old.password)
        old.address = record.get("address", old.address)
        old.zip_code = record.get("zip_code", old.zip_code)
        old.city = record.get("city", old.city)
        old.state = record.get("state", old.state)
        old.phone = record.get("phone", old.phone)
        db.session.commit()
    except Exception as ex:
        msg = f"Falha na a atualização de dados: {ex}"
    return msg


def delete_one(id):
    msg = "OK"
    try:
        record = Cliente.query.filter_by(id=id).first()
        db.session.delete(record)
        db.session.commit()
    except Exception as ex:
        msg = f"Falha na exclusão de dados: {ex}"
    return msg
