# pipreqs .\src --force

#######################################################################################################################
# DOCKER
#
# Habilitar api (projeto "brasilprev") via docker:
#
#   Na pasta raiz do projeto:
#
#   1. Para criar nova imagem
#      docker build -f brasilprev.DockerFile -t brasilprev-api-fargate-repository .
#
#   2. Executar o container:
#      docker container ls                          # para ver o nome do container que está em execucao
#      docker container stop nome_meu_container     # para parar a execução do container
#      docker container run -d -p 5000:5000 -v /Teles/Desenvolvimento/ProjetosGIT/Vita/VitaPy_api/src:/usr/share/vita-py-api vita-py-api-fargate-repository
#           nesse caso funcionou, mas pegou o código antivo. Ver depois...
#
#######################################################################################################################

import os
from waitress import serve
from index import app

# só dev:
os.environ['PORT'] = '5000'

serve(app, host='0.0.0.0', port=os.environ['PORT'])
