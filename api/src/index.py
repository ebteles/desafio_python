from flask_cors import CORS
from brasilprev.api_teste import app_brasilprev_bp as api_tes
from brasilprev.api_cliente import app_brasilprev_bp as api_cli
from brasilprev.api_produto import app_brasilprev_bp as api_pro
from brasilprev.api_pedido import app_brasilprev_bp as api_ped
from brasilprev.config import app

# registrar aqui cada módugo
app.register_blueprint(api_tes)   # bp = BluePrint
app.register_blueprint(api_cli)   # bp = BluePrint
app.register_blueprint(api_pro)   # bp = BluePrint
app.register_blueprint(api_ped)   # bp = BluePrint

# habilitar cors
cors = CORS(app, resources={r"/brasilprev/*": {"origins": "*"}})

if __name__ == '__main__':
    app.run()
