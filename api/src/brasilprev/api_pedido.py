from brasilprev.services import service_pedido
from flask import request, jsonify
from brasilprev import app_brasilprev_bp


##############################################################################################################
# PEDIDO
##############################################################################################################
@app_brasilprev_bp.route('/pedidos', methods=['GET', ])
def pedido_get_all():
    """
    Recupera dados de todos os usuários
    :return:
    """
    try:
        retorno = service_pedido.get_all()
        return jsonify(retorno)

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        return {
            "data": msg,
            "status": 500
        }


@app_brasilprev_bp.route('/pedidos/<string:id>', methods=['GET', ])
def pedido_get_one(id):
    """
    Recupera dados de um usuário
    :param id:
    :return:
    """
    try:
        retorno = service_pedido.get_one(id)
        return jsonify(retorno)

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        return {
            "data": msg,
            "status": 500
        }


@app_brasilprev_bp.route('/pedidos', methods=['POST', ])
def pedido_post_one():
    """
    Inclui um novo usuário
    :return:
    """
    try:
        data = request.get_json()
        result = service_pedido.post_one(data)
        return result

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        return {
            "data": msg,
            "status": 500
        }


@app_brasilprev_bp.route('/pedidos/itens', methods=['POST', ])
def pedido_item_post_one():
    """
    Inclui um novo usuário
    :return:
    """
    try:
        data = request.get_json()
        result = service_pedido.post_one_item(data)
        return result

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        return {
            "data": msg,
            "status": 500
        }


@app_brasilprev_bp.route('/pedidos', methods=['PUT', ])
def pedido_put_one():
    """
    Altera dados de um usuário
    :return:
    """
    try:
        data = request.get_json()
        result = service_pedido.put_one(data)
        return result

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        return {
            "data": msg,
            "status": 500
        }


@app_brasilprev_bp.route('/pedidos/<string:id>', methods=['DELETE', ])
def pedido_delete_one(id):
    """
    Exclui um usuário.
    :param id:
    :return:
    """
    try:
        return service_pedido.delete_one(id)

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        return {
            "data": msg,
            "status": 500
        }


@app_brasilprev_bp.route('/pedidos/itens/<string:id>', methods=['DELETE', ])
def pedido_item_delete_one(id):
    """
    Exclui um usuário.
    :param id:
    :return:
    """
    try:
        return service_pedido.delete_one_item(id)

    except Exception as e:
        msg = f"{type(e).__name__}: {e}"
        return {
            "data": msg,
            "status": 500
        }