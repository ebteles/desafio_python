import json
from brasilprev.models import db
from brasilprev.models.pedido import Pedido
from brasilprev.models.pedido_item import PedidoItem
from brasilprev.models.pedido_schema import pedido_schema
from brasilprev.models.pedido_item_schema import pedido_item_schema


def get_all():
    dados = Pedido.query.all()
    return json.loads(pedido_schema.dumps(dados))


def get_one(id):
    pd = Pedido.query.filter_by(id=id).first()
    pd_it = PedidoItem.query.filter_by(pedido_id=id)

    pd = json.loads(pedido_schema.dumps([pd]))
    if len(pd) > 0:
        pd = pd[0]

    dados = {
        "pedido": pd,
        "pedido_items": json.loads(pedido_item_schema.dumps(pd_it))
    }
    return dados


def post_one(record):
    msg = "OK"
    try:
        me = Pedido(**record)
        db.session.add(me)
        db.session.commit()
    except Exception as ex:
        msg = f"Falha ao inserir novo registro (pedido): {ex}"
    return msg


def post_one_item(record):
    msg = "OK"
    try:
        me = PedidoItem(**record)
        db.session.add(me)
        db.session.commit()
    except Exception as ex:
        msg = f"Falha ao inserir novo registro (pedido_item): {ex}"
    return msg


def put_one(record):
    msg = "OK"
    try:
        old = Pedido.query.filter_by(id=record["id"]).first()
        old.stock = record.get("stock", old.stock)
        db.session.commit()  # update only data_end

    except Exception as ex:
        msg = f"Falha na a atualização de dados: {ex}"
    return msg


def delete_one(id):
    msg = "OK"
    try:
        db.session.query(PedidoItem).filter_by(pedido_id=id).delete()
        db.session.query(Pedido).filter_by(id=id).delete()
        db.session.commit()
    except Exception as ex:
        msg = f"Falha na exclusão de dados (pedido): {ex}"
        db.session.rollback()
    return msg


def delete_one_item(id):
    msg = "OK"
    try:
        record = PedidoItem.query.filter_by(id=id).first()
        db.session.delete(record)
        db.session.commit()
    except Exception as ex:
        msg = f"Falha na exclusão de dados (pedido_item): {ex}"
    return msg